<?php

namespace Tests\Feature;

use Illuminate\Support\Str;
use Tests\TestCase;

class UndefinedRouteTest extends TestCase
{
    /**
     * Test user can see error if make request to undefined API route.
     *
     * @return void
     */
    public function testUserCanSeeErrorIfMakeRequestToUndefinedApiRoute()
    {
        $response = $this->getJson(Str::random(10));

        $response
            ->assertStatus(404)
            ->assertJson([
                'type' => 'Exception',
                'message' => 'Not found'
            ])
        ;
    }
}
