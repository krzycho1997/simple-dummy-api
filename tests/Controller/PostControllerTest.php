<?php

namespace Tests\Controller;

use App\Dto\Post;
use App\Exceptions\ExternalApiCallException;
use App\Services\Posts\Provider\PostProviderServiceInterface;
use Mockery;
use Tests\Helpers\Traits\PostTestDataTrait;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use PostTestDataTrait;

    /**
     * @const COUNT_OF_TEST_POSTS
     */
    private const COUNT_OF_TEST_POSTS = 5;

    /**
     * Test user can get post by IDs as JSON.
     *
     * @return void
     */
    public function testUserCanGetPostByIdAsJson(): void
    {
        $mockProviderServiceInterface = Mockery
            ::mock(PostProviderServiceInterface::class)
            ->shouldReceive('getPost')
            ->andReturnUsing(fn (int $id) => new Post($this->getArrayOfOnePost($id)))
            ->getMock();
        
        $this->app->instance(PostProviderServiceInterface::class, $mockProviderServiceInterface);
        
        $response = $this->getJson(route('posts.show', 1));
        
        $response
            ->assertStatus(200)
            ->assertJson($this->getArrayOfOnePost(1));
    }

    /**
     * Test User can't see post if post not exist as JSON.
     *
     * @return void
     */
    public function testUserCantSeePostIfPostNotExistAsJson(): void
    {
        $mockProviderServiceInterface = Mockery
            ::mock(PostProviderServiceInterface::class)
            ->shouldReceive('getPost')
            ->andReturnUsing(
                fn (int $id) =>
                $id > self::COUNT_OF_TEST_POSTS
                    ? new ExternalApiCallException()
                    : new Post($this->getArrayOfOnePost($id))
            )
            ->getMock();

        $this->app->instance(PostProviderServiceInterface::class, $mockProviderServiceInterface);

        $response = $this->getJson(route('posts.show', self::COUNT_OF_TEST_POSTS + 1));
        
        $response
            ->assertJson([]);
    }

    /**
     * Test user can get all posts as array.
     *
     * @return void
     */
    public function testUserCanGetAllPostsAsArray(): void
    {
        $mockProviderServiceInterface = Mockery
            ::mock(PostProviderServiceInterface::class)
            ->shouldReceive('getAllPosts')
            ->andReturnUsing(fn () => $this->getArrayOfPosts(self::COUNT_OF_TEST_POSTS))
            ->getMock();

        $this->app->instance(PostProviderServiceInterface::class, $mockProviderServiceInterface);

        $response = $this->getJson(route('posts.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->getArrayOfPosts(self::COUNT_OF_TEST_POSTS));
    }
}
