<?php

namespace Tests\Unit\Service;

use App\Services\Api\JsonPlaceHolder\JsonPlaceHolderService;
use App\Services\Api\JsonPlaceHolder\JsonPlaceHolderServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Tests\TestCase;

class JsonPlaceHolderApiServiceTest extends TestCase
{
    /**
     * @var JsonPlaceHolderServiceInterface $jsonPlaceHolderService
     */
    private JsonPlaceHolderServiceInterface $jsonPlaceHolderService;

    /**
     * @retrun void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->jsonPlaceHolderService = new JsonPlaceHolderService(app(Client::class));
    }

    /**
     * Test first post can be returned from jsonplaceholder as array.
     *
     * @throws GuzzleException|JsonException
     * @return void
     */
    public function testFirstPostsCanBeReturnedFromJsonPlaceHolderAsArray(): void
    {
        $this->withoutExceptionHandling();
        
        $this->assertIsArray(
            $this->jsonPlaceHolderService->getPostArrayByPostId(1)
        );
    }

    /**
     * Test all posts can be returned from jsonplaceholder as JSON array.
     *
     * @throws GuzzleException|JsonException
     * @return void
     */
    public function testAllPostsCanBeReturnedFromJsonPlaceHolderAsJsonArray(): void
    {
        $this->withoutExceptionHandling();
        
        $this->assertIsArray(
            $this->jsonPlaceHolderService->getAllPostsJsonArray()
        );
    }
}
