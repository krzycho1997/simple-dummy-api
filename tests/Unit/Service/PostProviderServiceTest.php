<?php

namespace Tests\Unit\Service;

use App\Dto\Post;
use App\Services\Posts\Handler\HandlerInterface;
use App\Services\Posts\Provider\PostProviderService;
use Mockery;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Tests\Helpers\Traits\PostTestDataTrait;
use Tests\TestCase;

class PostProviderServiceTest extends TestCase
{
    use PostTestDataTrait;

    /**
     * @const TEST_POST_ID
     */
    private const TEST_POST_ID = 1;

    /**
     * @const COUNT_OF_TEST_POSTS
     */
    private const COUNT_OF_TEST_POSTS = 5;

    /**
     * Test first post can be returned as PostDto object.
     *
     * @return void
     * @throws UnknownProperties
     */
    public function testFirstPostsCanBeReturnedAsPostDtoObject(): void
    {
        $this->withoutExceptionHandling();

        $mockHandlerInterface = Mockery
            ::mock(HandlerInterface::class)
            ->shouldReceive('getPost')
            ->andReturnUsing(fn (int $id) => new Post($this->getArrayOfOnePost($id)))
            ->getMock();

        $postProviderService = new PostProviderService(
            $mockHandlerInterface
        );

        $this->assertEquals(
            new Post($this->getArrayOfOnePost(self::TEST_POST_ID)),
            $postProviderService->getPost(self::TEST_POST_ID)
        );
    }

    /**
     * Test all posts can be returned as PostDto objects array.
     *
     * @return void
     * @throws UnknownProperties
     */
    public function testAllPostsCanBeReturnedAsPostDtoObjectsArray(): void
    {
        $this->withoutExceptionHandling();

        $mockHandlerInterface = Mockery
            ::mock(HandlerInterface::class)
            ->shouldReceive('getAllPosts')
            ->andReturnUsing(fn () => $this->getDtoArrayOfPosts(
                $this->getArrayOfPosts(self::COUNT_OF_TEST_POSTS)
            ))
            ->getMock();

        $postProviderService = new PostProviderService(
            $mockHandlerInterface
        );

        $this->assertEquals(
            $this->getDtoArrayOfPosts(
                $this->getArrayOfPosts(self::COUNT_OF_TEST_POSTS)
            ),
            $postProviderService->getAllPosts()
        );
    }
}
