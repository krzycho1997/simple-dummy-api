<?php

namespace Tests\Integration;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Tests\TestCase;

/**
 * Class JsonPlaceHolderTest
 * @package Tests\Integration
 */
class JsonPlaceHolderTest extends TestCase
{
    /**
     * @var Client $client
     */
    private Client $client;

    /**
     * @var string|null $jsonplaceholderUrl
     */
    private string|null $jsonplaceholderUrl;

    /**
     * @retrun void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->client = app(Client::class);
        $this->jsonplaceholderUrl = config('app.jsonplaceholder_url');
    }

    /**
     * Test jsonplaceholder can return first post as JSON.
     * @return void
     * @throws GuzzleException|JsonException
     */
    public function testJsonPlaceHolderCanReturnFirstPostAsJson(): void
    {
        $response = $this->client->request(
            'GET',
            $this->jsonplaceholderUrl . '/posts/1'
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getBody());
    }

    /**
     * Test jsonplaceholder can return all post as array.
     * @return void
     * @throws GuzzleException|JsonException
     */
    public function testJsonPlaceHolderCanReturnAllPostAsArray(): void
    {
        $response = $this->client->request(
            'GET',
            $this->jsonplaceholderUrl . '/posts'
        );

        $encodedResponse = json_decode(
            $response->getBody(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertIsArray($encodedResponse);
    }
}
