<?php


namespace Tests\Helpers\Traits;

use App\Dto\Post;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

/**
 * Trait ClientMockerTrait
 * @package Tests\Helpers\Traits
 */
trait PostTestDataTrait
{
    /**
     * @param int $id
     * @return array
     */
    #[ArrayShape(['userId' => "int", 'id' => "int", 'title' => "string", 'body' => "string"])]
    protected function getArrayOfOnePost(int $id): array
    {
        return [
            'userId' => $id,
            'id' => $id,
            'title' => 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
            'body' => 'suscipit recusandae consequuntur expedita et cum'
        ];
    }

    /**
     * @param int $countOfPosts
     * @return array
     */
    #[Pure]
    protected function getArrayOfPosts(int $countOfPosts): array
    {
        $posts = [];

        for ($i = 0; $i < $countOfPosts; $i++) {
            $posts[] = $this->getArrayOfOnePost($i + 1);
        }

        return $posts;
    }

    /**
     * @param array $postsArray
     * @return array
     * @throws UnknownProperties
     */
    protected function getDtoArrayOfPosts(array $postsArray): array
    {
        return array_map(fn ($value) => new Post($value), $postsArray);
    }
}
