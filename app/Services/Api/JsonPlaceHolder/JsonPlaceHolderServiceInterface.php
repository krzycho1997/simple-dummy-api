<?php


namespace App\Services\Api\JsonPlaceHolder;

/**
 * Interface JsonPlaceHolderServiceInterface
 * @package App\Services\Api\JsonPlaceHolder
 */
interface JsonPlaceHolderServiceInterface
{
    /**
     * @param int $postId
     * @return array
     */
    public function getPostArrayByPostId(int $postId): array;

    /**Array
     * @return array
     */
    public function getAllPostsJsonArray(): array;
}
