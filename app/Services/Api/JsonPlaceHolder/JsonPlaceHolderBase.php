<?php


namespace App\Services\Api\JsonPlaceHolder;

use GuzzleHttp\Client;

/**
 * Class JsonPlaceHolderBase
 * @package App\Services\Api\JsonPlaceHolder
 */
class JsonPlaceHolderBase
{
    /**
     * @var string|null $jsonplaceholderUrl
     */
    protected string|null $jsonplaceholderUrl;

    /**
     * JsonPlaceHolderBase constructor.
     * @param Client $client
     */
    public function __construct(
        protected Client $client
    ) {
        $this->jsonplaceholderUrl = config('app.jsonplaceholder_url');
    }
}
