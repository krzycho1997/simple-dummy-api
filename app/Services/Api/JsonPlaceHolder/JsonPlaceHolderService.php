<?php


namespace App\Services\Api\JsonPlaceHolder;

use GuzzleHttp\Exception\GuzzleException;
use JsonException;

/**
 * Class JsonPlaceHolderService
 * @package App\Services\Api\JsonPlaceHolder
 */
class JsonPlaceHolderService extends JsonPlaceHolderBase implements
    JsonPlaceHolderServiceInterface
{
    /**
     * @param int $postId
     * @return array
     * @throws GuzzleException|JsonException
     */
    public function getPostArrayByPostId(int $postId): array
    {
        $response = $this->client->get(
            $this->jsonplaceholderUrl . '/posts/' . $postId,
        );

        return json_decode(
            $response->getBody(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }

    /**
     * @return array
     * @throws GuzzleException|JsonException
     */
    public function getAllPostsJsonArray(): array
    {
        $response = $this->client->get(
            $this->jsonplaceholderUrl . '/posts'
        );

        return json_decode(
            $response->getBody(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }
}
