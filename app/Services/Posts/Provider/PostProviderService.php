<?php


namespace App\Services\Posts\Provider;

use App\Dto\Post;

/**
 * Class PostProviderService
 * @package App\Services\Posts\Provider
 */
class PostProviderService extends PostProviderServiceBase implements
    PostProviderServiceInterface
{
    /**
     * @param int $postId
     * @return Post
     */
    public function getPost(int $postId): Post
    {
        return $this->handler->getPost($postId);
    }

    /**
     * @return array
     */
    public function getAllPosts(): array
    {
        return $this->handler->getAllPosts();
    }
}
