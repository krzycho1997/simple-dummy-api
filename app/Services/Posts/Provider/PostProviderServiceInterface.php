<?php


namespace App\Services\Posts\Provider;

use App\Dto\Post;

/**
 * Interface PostProviderServiceInterface
 * @package App\Services\Posts\Provider
 */
interface PostProviderServiceInterface
{
    /**
     * @param int $postId
     *
     * @return Post
     */
    public function getPost(int $postId): Post;

    /**
     * @return Post[]
     */
    public function getAllPosts(): array;
}
