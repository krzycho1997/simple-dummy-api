<?php


namespace App\Services\Posts\Provider;

use App\Services\Posts\Handler\HandlerInterface;

/**
 * Class PostProviderServiceBase
 * @package App\Services\Posts\Provider
 */
class PostProviderServiceBase
{
    /**
     * PostProviderServiceBase constructor.
     * @param HandlerInterface $handler
     */
    public function __construct(
        protected HandlerInterface $handler
    ) {
    }
}
