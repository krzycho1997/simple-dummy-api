<?php


namespace App\Services\Posts\Handler;

use App\Dto\Post;
use App\Services\Api\JsonPlaceHolder\JsonPlaceHolderServiceInterface;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

/**
 * Class PostHandlerServiceBase
 * @package App\Services\Posts\Handler
 */
class PostHandlerServiceBase
{
    /**
     * PostHandlerServiceBase constructor.
     * @param JsonPlaceHolderServiceInterface $jsonPlaceHolderService
     */
    public function __construct(
        protected JsonPlaceHolderServiceInterface $jsonPlaceHolderService
    ) {
    }

    /**
     * @param array $postsArray
     * @return array
     * @throws UnknownProperties
     */
    protected function getDtoPostsArray(array $postsArray): array
    {
        return array_map(fn ($value) => new Post($value), $postsArray);
    }
}
