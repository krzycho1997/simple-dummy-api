<?php


namespace App\Services\Posts\Handler;

use App\Dto\Post;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

/**
 * Class PostHandlerService
 * @package App\Services\Posts\Handler
 */
class PostHandlerService extends PostHandlerServiceBase implements
    HandlerInterface
{
    /**
     * @param int $postId
     * @return Post
     * @throws UnknownProperties
     */
    public function getPost(int $postId): Post
    {
        $postArray = $this
            ->jsonPlaceHolderService
            ->getPostArrayByPostId($postId)
        ;

        return new Post($postArray);
    }

    /**
     * @return array
     * @throws UnknownProperties
     */
    public function getAllPosts(): array
    {
        $postArray = $this
            ->jsonPlaceHolderService
            ->getAllPostsJsonArray()
        ;

        return $this->getDtoPostsArray($postArray);
    }
}
