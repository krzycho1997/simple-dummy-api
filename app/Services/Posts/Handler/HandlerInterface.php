<?php


namespace App\Services\Posts\Handler;

use App\Dto\Post;

interface HandlerInterface
{
    /**
     * @param int $postId
     *
     * @return Post
     */
    public function getPost(int $postId): Post;

    /**
     * @return Post[]
     */
    public function getAllPosts(): array;
}
