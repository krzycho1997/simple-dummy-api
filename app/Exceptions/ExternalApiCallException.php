<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use ReflectionClass;

/**
 * Class ExternalApiCallException
 * @package App\Exceptions
 */
class ExternalApiCallException extends Exception
{
    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        $isPostNotFoundError =
            Str::contains($this->getMessage(), '404 Not Found');
                             
        return response()
            ->json([
                'type' => (new ReflectionClass($this))->getShortName(),
                'message' => $isPostNotFoundError ? 'Post not found' : 'Undefined External API Error'
            ], $isPostNotFoundError ? 404 : 500);
    }
}
