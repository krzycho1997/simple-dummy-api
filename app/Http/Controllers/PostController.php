<?php

namespace App\Http\Controllers;

use App\Exceptions\ExternalApiCallException;
use App\Http\Requests\PostRequest;
use App\Services\Posts\Provider\PostProviderServiceInterface;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * PostController constructor.
     * @param PostProviderServiceInterface $postProviderService
     */
    public function __construct(
        private PostProviderServiceInterface $postProviderService
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws ExternalApiCallException
     *
     * @OA\Get(
     *     path="/api/posts",
     *     summary="Get all posts",
     *     tags={"posts"},
     *     description="Get all available posts as JSON array",
     *     operationId="allPosts",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             type="array",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid tag value",
     *     ),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            return response()->json($this->postProviderService->getAllPosts());
        } catch (Exception) {
            throw new ExternalApiCallException();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param PostRequest $request
     * @return JsonResponse
     * @throws ExternalApiCallException
     *
     * @OA\Get(
     *     path="/api/posts/{postId}",
     *     summary="Find post by ID",
     *     description="Returns a single pet",
     *     operationId="getPost",
     *     tags={"post"},
     *     @OA\Parameter(
     *         description="ID of post to return",
     *         in="path",
     *         name="postId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Post"),
     *         )
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Not found"
     *     ),
     * )
     */
    public function show(PostRequest $request): JsonResponse
    {
        try {
            return response()->json($this->postProviderService->getPost($request->post));
        } catch (Exception $exception) {
            throw new ExternalApiCallException($exception);
        }
    }
}
