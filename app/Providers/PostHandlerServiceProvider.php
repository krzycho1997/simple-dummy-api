<?php

namespace App\Providers;

use App\Services\Posts\Handler\HandlerInterface;
use App\Services\Posts\Handler\PostHandlerService;
use Illuminate\Support\ServiceProvider;

class PostHandlerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            HandlerInterface::class,
            PostHandlerService::class
        );
    }
}
