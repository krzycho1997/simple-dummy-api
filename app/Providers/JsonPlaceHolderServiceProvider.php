<?php

namespace App\Providers;

use App\Services\Api\JsonPlaceHolder\JsonPlaceHolderService;
use App\Services\Api\JsonPlaceHolder\JsonPlaceHolderServiceInterface;
use Illuminate\Support\ServiceProvider;

class JsonPlaceHolderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            JsonPlaceHolderServiceInterface::class,
            JsonPlaceHolderService::class
        );
    }
}
