<?php

namespace App\Providers;

use App\Services\Posts\Provider\PostProviderService;
use App\Services\Posts\Provider\PostProviderServiceInterface;
use Illuminate\Support\ServiceProvider;

class PostProviderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            PostProviderServiceInterface::class,
            PostProviderService::class
        );
    }
}
