<?php


namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class PostDto
 * @package App\Dto
 * @OA\Schema()
 */
class Post extends DataTransferObject
{
    /**
     * @var int $userId
     * @OA\Property()
     */
    public int $userId;

    /**
     * @var int $id
     * @OA\Property()
     */
    public int $id;

    /**
     * @var string $title
     * @OA\Property()
     */
    public string $title;

    /**
     * @var string $body
     * @OA\Property()
     */
    public string $body;
}
