﻿# Welcome to simple-dummy-app!

This is a simple API app for getting posts from external source ({JSON} Placeholder). App was written thanks to Laravel 8 with PHP 8.


## Requirements

 - Docker 
 - Some free space on your disk

## Installation

 1. Create .env from example:

    `$ cp .env.example .env`

 2. Install application dependencies: 
 
	 `$ docker run --rm -u "$(id -u):$(id -g)" -v $(pwd):/opt -w /opt laravelsail/php80-composer:latest composer install --ignore-platform-reqs`
 
 2. Run container:
 
	 `$ ./vendor/bin/sail up`


## API documentation

	<url>/api/documentation

## Tests

    $ ./vendor/bin/sail php artisan test
