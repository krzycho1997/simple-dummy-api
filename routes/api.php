<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group.Ż Enjoy building your API!
|
*/

Route::resource('posts', PostController::class)->only([
    'index', 'show'
]);

Route::fallback(function () {
    return response()->json([
        'type' => 'Exception',
        'message' => 'Not found'
    ], 404);
});
